﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace filters
{
    public partial class Form1 : Form
    {
        Bitmap image = null;
        int[,] kernel = null;
        int FormHeight, FormWidth;

        Stack<Bitmap> stack = new Stack<Bitmap>(100);

        public Form1()
        {
            InitializeComponent();

            FormHeight = Height;
            FormWidth = Width;
        }

        private void inversionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //InvertFilter filter = new InvertFilter();
            //Bitmap resultImage = filter.processImage(image);
            //pictureBox1.Image = resultImage;
            //pictureBox1.Refresh();
            Filters filter = new InvertFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Bitmap newImage = ((Filters)e.Argument).processImage(image, backgroundWorker1);
            if (backgroundWorker1.CancellationPending != true) image = newImage;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled) {
                pictureBox1.Image = image;
                pictureBox1.Refresh();
            }
            progressBar1.Value = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

        private void blurFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new BlurFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void gaussianFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new GaussianFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void grayScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new GrayScaleFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void sepiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new SepiaFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void brightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new BrightFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void sobelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new SobelFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void sobelNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new SobelFilterNew();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void harshnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new HarshnessFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void embrassingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new EmbossingFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void medianFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new MedianFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void luminousEdgesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new LuminousEdgesFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void shiftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new ShiftFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void rotateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new RotationFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void waves1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new WavesFilterFirst();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void waves2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new WavesFilterSecond();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void motionBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MotionBlurFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void glassEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new GlassEffectFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void sharpnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new SharpnessFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void blackAndWhiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new BlackAndWhiteColorsFilter(image);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void expansionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new ExpansionOperation(kernel);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void erosionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new ErosionOperation(kernel);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new OpenOperation(kernel);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new CloseOperation(kernel);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void topHatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new TopHatFilter(kernel);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Image files | *.png; *.jpg; *.bmp; | All Files | *.*";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                image = new Bitmap(fileDialog.FileName);
                pictureBox1.Image = image;
                pictureBox2.Image = image;
                pictureBox1.Refresh();
                pictureBox2.Refresh();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog.Title = "Save an Image File";
            saveFileDialog.ShowDialog();
 
            if (saveFileDialog.FileName != "")
            {
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog.OpenFile();

                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        this.image.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        this.image.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        this.image.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }

                fs.Close();
            }
        }

        private void linearStretchingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new LiniarStretchingFilter(image);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void kernelButton_Click(object sender, EventArgs e)
        {
            char[] kernelStr = kernelTextBox.Text.ToCharArray();
            int i = 0, j = 0, k = 0;
            int size = (int)Math.Sqrt((kernelTextBox.TextLength + 2) / 3);

            kernel = new int[size, size];

            tb.Text = "size = " + Convert.ToString(size);

            while (i < kernelTextBox.TextLength) {
                if (kernelStr[i] == ','){
                    k++;
                    if (k == size) {
                        j++;
                        k = 0;
                    }
                }

                int l = 0;
                string number = "0";
                while (i + l < kernelTextBox.TextLength) {
                    if ((kernelStr[i + l] <= '9') && (kernelStr[i + l] >= '0'))
                    {
                        number += kernelStr[i + l];
                        l++;
                    }
                    else break;
                }
                kernel[j, k] =  (int)Convert.ToInt32(number);

                i++;
            }
        }

        private void kernelTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void greyWorldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new GreyWorldFilter(image);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void idealReflectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new IdealReflectorFilter(image);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void correctionWithSupportElementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int R = 128, G = 128, B = 128;
            int x = 0, y = 0;

            //tbR.Visible = true;
            //tbG.Visible = true;
            //tbB.Visible = true;
            //tbX.Visible = true;
            //tbY.Visible = true;

            //RLabel.Visible = true;
            //GLabel.Visible = true;
            //BLabel.Visible = true;
            //xLabel.Visible = true;
            //yLabel.Visible = true;

            R = Convert.ToInt32(tbR.Text);
            G = Convert.ToInt32(tbG.Text);
            B = Convert.ToInt32(tbB.Text);

            x = Convert.ToInt32(tbX.Text);
            y = Convert.ToInt32(tbY.Text);

            if (R > 255) R = 255; else if (R < 0) R = 0;
            if (G > 255) G = 255; else if (G < 0) G = 0;
            if (B > 255) B = 255; else if (B < 0) B = 0;

            if ((x > image.Width) || (x < 0)) x = 0;
            if ((y > image.Height) || (y < 0)) y = 0;

            Filters filter = new CorrectionWithSupportElementFilter(image, x, y, R, G, B);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void filtersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (image != null)
            {
                stack.Push(image);
            }
        }

        private void undo_btn_Click(object sender, EventArgs e)
        {
            if (stack.Count != 0) {
                image = stack.Pop();
                pictureBox1.Image = image;
                pictureBox2.Image = image;
            }
        }

        private void newFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new NewFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void newestFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new NewestFilter(image);
            backgroundWorker1.RunWorkerAsync(filter);
        }
    }
}
