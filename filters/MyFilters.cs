﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;

namespace filters
{
    abstract class Filters
    {
        protected abstract Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0);

        public virtual Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker) {
            Bitmap resultImage = new Bitmap(sourceImage.Width, sourceImage.Height);
            for (int i = 0; i < sourceImage.Width; i++) {
                worker.ReportProgress((int)((float)i / resultImage.Width * 100));
                if (worker.CancellationPending) return null;

                for (int j = 0; j < sourceImage.Height; j++) {
                    resultImage.SetPixel(i, j, calculateNewPixelColor(sourceImage, i, j));
                }
            }

            return resultImage;
        }

        public int Clamp(int value, int min, int max) {
            if (value < min) return min;
            if (value > max) return max;
            return value;
        }
    };

    class InvertFilter : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            Color resultColor = Color.FromArgb(255 - sourceColor.R, 255 - sourceColor.G, 255 - sourceColor.B);

            return resultColor;
        }
    };

    class MatrixFilter : Filters
    {
        protected float[,] kernel = null;
        protected MatrixFilter() { }
        public MatrixFilter(float[,] kernel) {
            this.kernel = kernel;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int radiusX = kernel.GetLength(0) / 2;
            int radiusY = kernel.GetLength(1) / 2;

            float resultR = 0;
            float resultG = 0;
            float resultB = 0;

            for (int l = -radiusY; l <= radiusY; l++) {
                for (int k = -radiusX; k <= radiusX; k++) {
                    int idX = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idY = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idX, idY);

                    resultR += neighborColor.R * kernel[k + radiusX, l + radiusY];
                    resultG += neighborColor.G * kernel[k + radiusX, l + radiusY];
                    resultB += neighborColor.B * kernel[k + radiusX, l + radiusY];
                }
            }
            return Color.FromArgb(
                Clamp((int)resultR + offset, 0, 255),
                Clamp((int)resultG + offset, 0, 255),
                Clamp((int)resultB + offset, 0, 255)
                );
        }
    };

    class BlurFilter : MatrixFilter{
        public BlurFilter() {
            int sizeX = 3;
            int sizeY = 3;
            kernel = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++) {
                for (int j = 0; j < sizeY; j++)
                    kernel[i, j] = 1.0f / (float)(sizeX * sizeY);
            }
        }
    };

    class GaussianFilter : MatrixFilter {
        public GaussianFilter() {
            createGaussianKernel(3, 2);
        }
        public void createGaussianKernel(int radius, float sigma) {
            int size = 2 * radius + 1;
            kernel = new float[size, size];
            float norm = 0;

            for (int i = -radius; i <= radius; i++)
                for (int j = -radius; j <= radius; j++) {
                    kernel[i + radius, j + radius] = (float)(Math.Exp(-(i * i + j * j) / (sigma * sigma)));
                    norm += kernel[i + radius, j + radius];
                }
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    kernel[i, j] /= norm;
        }
    }

    class GrayScaleFilter : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int Intensity = (int)(0.36 * sourceColor.R + 0.53 * sourceColor.G + 0.11 * sourceColor.B);
            Color resultColor = Color.FromArgb(Intensity, Intensity, Intensity);

            return resultColor;
        }
    }

    class SepiaFilter : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int R = sourceColor.R,
                G = sourceColor.G,
                B = sourceColor.B;
            double Intensity = 0.36 * R + 0.53 * G + 0.11 * B;
            Color resultColor = Color.FromArgb(
                Clamp((int)(Intensity + 2), 0, 255),
                Clamp((int)(Intensity + 0.5), 0, 255),
                Clamp((int)(Intensity - 1), 0, 255)
                );

            return resultColor;
        }
    }

    class BrightFilter : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 30)
        {
            return Color.FromArgb(
                Clamp(sourceImage.GetPixel(x, y).R + offset, 0, 255),
                Clamp(sourceImage.GetPixel(x, y).G + offset, 0, 255),
                Clamp(sourceImage.GetPixel(x, y).B + offset, 0, 255)
                );
        }
    }

    class SobelFilterNew : MatrixFilter {
        protected float[,] GX, GY;
        public SobelFilterNew() {
            GX = new float[,] {
            { -1, 0, 1 },
            { -2, 0, 2 },
            { -1, 0, 1 }
        };
            GY = new float[,] {
            { -1, -2, -1 },
            { 0, 0, 0 },
            { 1, 2, 1 }
        };
            //kernel = GX;
        }

        const int radiusX = 3 / 2;
        const int radiusY = 3 / 2;

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            float resultRx = 0;
            float resultGx = 0;
            float resultBx = 0;

            float resultRy = 0;
            float resultGy = 0;
            float resultBy = 0;

            for (int l = -radiusY; l <= radiusY; l++)
            {
                for (int k = -radiusX; k <= radiusX; k++)
                {
                    int idX = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idY = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idX, idY);

                    resultRx += neighborColor.R * GX[k + radiusX, l + radiusY];
                    resultGx += neighborColor.G * GX[k + radiusX, l + radiusY];
                    resultBx += neighborColor.B * GX[k + radiusX, l + radiusY];
                }
            }

            for (int l = -radiusY; l <= radiusY; l++)
            {
                for (int k = -radiusX; k <= radiusX; k++)
                {
                    int idX = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idY = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idX, idY);

                    resultRy += neighborColor.R * GY[k + radiusX, l + radiusY];
                    resultGy += neighborColor.G * GY[k + radiusX, l + radiusY];
                    resultBy += neighborColor.B * GY[k + radiusX, l + radiusY];
                }
            }

            int result = Clamp((int)(Math.Sqrt(resultRx * resultRx + resultRy * resultRy + resultBx * resultBx + resultBy * resultBy + resultGx * resultGx + resultGy * resultGy)), 0, 255);

            return Color.FromArgb(result, result, result);
        }
    }

    class SobelFilter : MatrixFilter {
        protected float[,] GX = new float[,] {
            { -1, 0, 1 }, 
            { -2, 0, 2 },
            { -1, 0, 1 }
        }, 
            GY = new float[,] {
            { -1, -2, -1 },
            { 0, 0, 0 },
            { 1, 2, 1 }
        };
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Bitmap tmp = sourceImage;
            kernel = GX;
            Color colorX = base.calculateNewPixelColor(tmp, x, y);

            kernel = GY;
            Color colorY = base.calculateNewPixelColor(sourceImage, x, y);

            float rx = colorX.R, gx = colorX.G, bx = colorX.B,
                ry = colorY.R, gy = colorY.G, by = colorY.B;

            int result = Clamp(
                (int)Math.Sqrt(rx * rx + ry * ry + gx * gx + gy * gy + bx * bx + by * by),
                0,
                255
                );

            return Color.FromArgb(result, result, result);          
           
       
        }
    }

    class HarshnessFilter : MatrixFilter {
        public HarshnessFilter() {
            kernel = new float[,] {
                { 0, -1, 0 },
                { -1, 5, -1},
                { 0, -1, 0 }
            };
        }
    }

    class EmbossingFilter : MatrixFilter {
        public EmbossingFilter() {
            kernel = new float[,] {
                { 0, 1, 0},
                { 1, 0, -1},
                { 0, -1, 0}
            };
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int radiusX = kernel.GetLength(0) / 2;
            int radiusY = kernel.GetLength(1) / 2;

            float resultR = 0;
            float resultG = 0;
            float resultB = 0;

            for (int l = -radiusY; l <= radiusY; l++)
            {
                for (int k = -radiusX; k <= radiusX; k++)
                {
                    int idX = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idY = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idX, idY);

                    resultR += neighborColor.R * kernel[k + radiusX, l + radiusY];
                    resultG += neighborColor.G * kernel[k + radiusX, l + radiusY];
                    resultB += neighborColor.B * kernel[k + radiusX, l + radiusY];
                }
            }
            return Color.FromArgb(
                Clamp((int)resultR + 128, 0, 255),
                Clamp((int)resultG + 128, 0, 255),
                Clamp((int)resultB + 128, 0, 255)
                );
        }
    }

    class MedianFilter : Filters {
        protected int radius;
        int colorArraySize;
        public MedianFilter(int _radius = 3) {
            radius = _radius;
            colorArraySize = (2 * radius + 1) * (2 * radius + 1);
        }        

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color[] ColorArray = new Color[colorArraySize];
            for (int dx = - radius; dx <= radius; dx++)
                for (int dy = -radius; dy <= radius; dy++) {
                    ColorArray[(radius + dx) * (2 * radius + 1) + radius + dy] = sourceImage.GetPixel(Clamp(x + dx, 0, sourceImage.Width - 1), Clamp(y + dy, 0, sourceImage.Height - 1));
                }
            for(int i = 0; i < colorArraySize; i++)
                for(int j = i; j < colorArraySize; j++){
                    
                    int Intensity1 = (int)(0.36 * ColorArray[i].R + 0.53 * ColorArray[i].G + 0.11 * ColorArray[i].B);
                    int Intensity2 = (int)(0.36 * ColorArray[j].R + 0.53 * ColorArray[j].G + 0.11 * ColorArray[j].B);
                if(Intensity2 < Intensity1) {
                    Color tmp = ColorArray[i];
                    ColorArray[i] = ColorArray[j];
                    ColorArray[j] = tmp;
                }
            }
            return ColorArray[colorArraySize / 2];
        }
    }

    class LuminousEdgesFilter : MatrixFilter {
        protected int radius;
        int colorArraySize;

        protected float[,] GX = new float[,] {
            { -1, 0, 1 },
            { -2, 0, 2 },
            { -1, 0, 1 }
        },
            GY = new float[,] {
            { -1, -2, -1 },
            { 0, 0, 0 },
            { 1, 2, 1 }
        };
        public LuminousEdgesFilter(int _radius = 3)
        {
            radius = _radius;
            colorArraySize = (2 * radius + 1) * (2 * radius + 1);
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color[] ColorArray = new Color[colorArraySize];
            for (int dx = -radius; dx <= radius; dx++)
                for (int dy = -radius; dy <= radius; dy++)
                {
                    ColorArray[(radius + dx) * (2 * radius + 1) + radius + dy] = sourceImage.GetPixel(Clamp(x + dx, 0, sourceImage.Width - 1), Clamp(y + dy, 0, sourceImage.Height - 1));
                }
            for (int i = 0; i < colorArraySize; i++)
                for (int j = i; j < colorArraySize; j++)
                {

                    int Intensity1 = (int)(0.36 * ColorArray[i].R + 0.53 * ColorArray[i].G + 0.11 * ColorArray[i].B);
                    int Intensity2 = (int)(0.36 * ColorArray[j].R + 0.53 * ColorArray[j].G + 0.11 * ColorArray[j].B);
                    if (Intensity2 < Intensity1)
                    {
                        Color tmp = ColorArray[i];
                        ColorArray[i] = ColorArray[j];
                        ColorArray[j] = tmp;
                    }
                }

            Bitmap temp = sourceImage;
            temp.SetPixel(x, y, ColorArray[colorArraySize / 2]);
            kernel = GX;
            Color colorX = base.calculateNewPixelColor(temp, x, y);

            kernel = GY;
            Color colorY = base.calculateNewPixelColor(temp, x, y);

            float rx = colorX.R, gx = colorX.G, bx = colorX.B,
                ry = colorY.R, gy = colorY.G, by = colorY.B;

            int result = Clamp(
                (int)Math.Sqrt(rx * rx + ry * ry + gx * gx + gy * gy + bx * bx + by * by),
                0,
                255
                );

            return Color.FromArgb(result, result, result);
        }
    }

    class ShiftFilter : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            if (x < sourceImage.Width - 50) return sourceImage.GetPixel(x + 50, y);
            return Color.FromArgb(0, 0, 0); 
        }
    }

    class RotationFilter : Filters {
        double angle;
        int x0, y0;
        public RotationFilter(int tx0 = 0, int ty0 = 0, double tAngle = Math.PI / 4) {
            angle = tAngle;
            x0 = tx0;
            y0 = ty0;
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color result;
            double xr = (x - x0) * Math.Cos(angle) - (y - y0) * Math.Sin(angle) + x0;
            double yr = (x - x0) * Math.Sin(angle) + (y - y0) * Math.Cos(angle) + y0;

            if ((xr < sourceImage.Width) &&
                (xr > 0) &&
                (yr < sourceImage.Height) &&
                (yr > 0)) result = sourceImage.GetPixel((int)xr, (int)yr);
            else result = Color.FromArgb(0, 0, 0);

            return result;
        }
    }

    class WavesFilterFirst : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            double resultx = x + 20 * Math.Sin(2 * Math.PI * y / 60);

            if ((resultx < sourceImage.Width) && (resultx > -1)) return sourceImage.GetPixel((int)resultx, y);
            else return sourceImage.GetPixel((int)Math.Abs(resultx) % sourceImage.Width, y);
        }
    }

    class WavesFilterSecond : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            double resultx = x + 20 * Math.Sin(2 * Math.PI * y / 30);

            if ((resultx < sourceImage.Width) && (resultx > -1)) return sourceImage.GetPixel((int)resultx, y);
            //else return sourceImage.GetPixel((int)Math.Abs(resultx) % sourceImage.Width, y);
            else return sourceImage.GetPixel(x, y);
        }
    }

    class MotionBlurFilter : MatrixFilter {
        const int kernelSize = 7;
        public MotionBlurFilter() {
            kernel = new float[kernelSize, kernelSize];
            for (int i = 0; i < kernelSize; i++)
                for (int j = 0; j < kernelSize; j++)
                    if (i == j) kernel[i, j] = 1.0f / (float)kernelSize;
                    else kernel[i, j] = 0.0f;
        }
    }

    class GlassEffectFilter : Filters {
        Random rand;
        public GlassEffectFilter() {
            rand = new Random();
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            return sourceImage.GetPixel(
                Clamp((int)(x + (rand.NextDouble() - 0.5) * 10), 0, sourceImage.Width - 1),
                Clamp((int)(y + (rand.NextDouble() - 0.5) * 10), 0, sourceImage.Height - 1)
                );
        }
    }

    class SharpnessFilter : MatrixFilter {
        public SharpnessFilter() {
            kernel = new float[,]{
                { -1, -1, -1 }, 
                { -1, 9, -1 }, 
                { -1, -1, -1}
            };
        }
    }

    class BlackAndWhiteColorsFilter : Filters {
        protected int AverageIntensity = 128;
        public BlackAndWhiteColorsFilter(Bitmap srcImage = null) {
            if (srcImage != null) {
                int ImHeight = srcImage.Height;
                int ImWidth = srcImage.Width;
                double Sum = 0;

                for (int i = 0; i < ImWidth; i++)
                    for (int j = 0; j < ImHeight; j++) {
                        double Intensity = (0.36 * srcImage.GetPixel(i, j).R + 0.53 * srcImage.GetPixel(i, j).G + 0.11 * srcImage.GetPixel(i, j).B);
                        Sum += Intensity;
                    }
                AverageIntensity = (int)(Sum / (double)(ImWidth * ImHeight));
            }
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0) { 
            int Intensity = (int)(( 0.36 * sourceImage.GetPixel(x, y).R + 0.53 * sourceImage.GetPixel(x, y).G + 0.11 * sourceImage.GetPixel(x, y).B));
            //int Intensity = (int)((sourceImage.GetPixel(x, y).R + sourceImage.GetPixel(x, y).G + sourceImage.GetPixel(x, y).B ) / 3);

            if (Intensity > AverageIntensity) return Color.FromArgb(0, 0, 0);
            else return Color.FromArgb(255, 255, 255);
        }
    }

    // dilation = expansion 
    class ExpansionOperation : Filters {
        protected int[,] kernel;
        private int radius; 
        public ExpansionOperation(int[,] _kernel = null)
        {
            if (_kernel == null)
            {
                kernel = new int[,] {
                { 0, 0, 1, 1, 1, 0, 0},
                { 0, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 0, 1, 1, 1, 1, 1, 0},
                { 0, 0, 1, 1, 1, 0, 0}
                };
            }
            else kernel = _kernel;

            radius = kernel.GetLength(0) / 2;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int max = 0, indexX = 0, indexY = 0, pixel = 0;
            int ImageWidth = sourceImage.Width, ImageHeight = sourceImage.Height;
            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++) {
                    pixel = sourceImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel > max)) max = pixel;
                }
            return Color.FromArgb(max, max, max);
        }
    }

    class ErosionOperation : Filters {
        protected int[,] kernel;
        private int radius;
        public ErosionOperation(int[,] _kernel = null)
        {
            if (_kernel == null)
            {
                kernel = new int[,] {
                { 0, 0, 1, 1, 1, 0, 0},
                { 0, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 0, 1, 1, 1, 1, 1, 0},
                { 0, 0, 1, 1, 1, 0, 0}
                };
            }
            else kernel = _kernel;

            radius = kernel.GetLength(0) / 2;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int min = 255, indexX = 0, indexY = 0, pixel = 0;
            int ImageWidth = sourceImage.Width, ImageHeight = sourceImage.Height;
            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++)
                {
                    pixel = sourceImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel < min)) min = pixel;
                }
            return Color.FromArgb(min, min, min);
        }
    
    }

    class OpenOperation : Filters {
        protected int[,] kernel;
        private int radius;
        public OpenOperation(int[,] _kernel = null)
        {
            if (_kernel == null)
            {
                kernel = new int[,] {
                { 0, 0, 1, 1, 1, 0, 0},
                { 0, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 0, 1, 1, 1, 1, 1, 0},
                { 0, 0, 1, 1, 1, 0, 0}
                };
            }
            else kernel = _kernel;

            radius = kernel.GetLength(0) / 2;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int min = 255, indexX = 0, indexY = 0, pixel = 0;
            int ImageWidth = sourceImage.Width, ImageHeight = sourceImage.Height;
            Bitmap tmpImage = sourceImage;
            int max = 0;

            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++)
                {
                    pixel = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel < min)) min = pixel;
                }

            tmpImage.SetPixel(x, y, Color.FromArgb(min, min, min));

            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++)
                {
                    pixel = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel > max)) max = pixel;
                }

            return Color.FromArgb(max, max, max);
        }
    }

    class CloseOperation : Filters
    {
        protected int[,] kernel;
        private int radius;
        public CloseOperation(int[,] _kernel = null)
        {
            if (_kernel == null)
            {
                kernel = new int[,] {
                { 0, 0, 1, 1, 1, 0, 0},
                { 0, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 0, 1, 1, 1, 1, 1, 0},
                { 0, 0, 1, 1, 1, 0, 0}
                };
            }
            else kernel = _kernel;

            radius = kernel.GetLength(0) / 2;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int min = 255, indexX = 0, indexY = 0, pixel = 0;
            int ImageWidth = sourceImage.Width, ImageHeight = sourceImage.Height;
            Bitmap tmpImage = sourceImage;
            int max = 0;

            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++)
                {
                    pixel = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel > max)) max = pixel;
                }

            tmpImage.SetPixel(x, y, Color.FromArgb(max, max, max));

            for (int k = -radius; k <= radius; k++)
                for (int l = -radius; l <= radius; l++)
                {
                    pixel = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1)).R;
                    indexX = radius + k;
                    indexY = radius + l;

                    if ((kernel[indexX, indexY] == 1) && (pixel < min)) min = pixel;
                }

            return Color.FromArgb(min, min, min);
        }
    }

    class TopHatFilter : Filters {
        protected int[,] kernel;
        private int radius;
        protected bool errosing = true;
        public TopHatFilter(int[,] _kernel = null)
        {
            if (_kernel == null)
            {
                kernel = new int[,] {
                { 0, 0, 1, 1, 1, 0, 0},
                { 0, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 1, 1, 1, 1, 1, 1, 1},
                { 0, 1, 1, 1, 1, 1, 0},
                { 0, 0, 1, 1, 1, 0, 0}
                };
            }
            else kernel = _kernel;

            radius = kernel.GetLength(0) / 2;
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            int indexX = 0, indexY = 0;
            double pixel = 0.0;
            int ImageWidth = sourceImage.Width, ImageHeight = sourceImage.Height;
            Bitmap tmpImage = sourceImage;
            Color color = Color.FromArgb(0, 0, 0);
            Color ColorMax = Color.FromArgb(0, 0, 0);
            Color ColorMin = Color.FromArgb(0, 0, 0);
            double max = 0.0;
            double min = 255.0;

            if (errosing)
            {
                for (int k = -radius; k <= radius; k++)
                    for (int l = -radius; l <= radius; l++)
                    {
                        color = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1));
                        pixel = (0.36 * color.R + 0.53 * color.G + 0.11 * color.B);
                        //pixel = (color.R + color.G + color.B) / 3.0;
                        indexX = radius + k;
                        indexY = radius + l;

                        if ((kernel[indexX, indexY] == 1) && (pixel > max))
                        {
                            max = pixel;
                            ColorMax = color;
                        }
                    }

                return Color.FromArgb(
                        ColorMax.R,
                        ColorMax.G,
                        ColorMax.B
                    );
            }
            else
            {
                for (int k = -radius; k <= radius; k++)
                    for (int l = -radius; l <= radius; l++)
                    {
                        color = tmpImage.GetPixel(Clamp(x + k, 0, ImageWidth - 1), Clamp(y + l, 0, ImageHeight - 1));
                        pixel = (0.36 * color.R + 0.53 * color.G + 0.11 * color.B);
                        pixel = (color.R + color.G + color.B) / 3.0;
                        indexX = radius + k;
                        indexY = radius + l;

                        if ((kernel[indexX, indexY] == 1) && (pixel < min))
                        {
                            min = pixel;
                            ColorMin = color;
                        }
                    }

                return Color.FromArgb(
                        ColorMin.R,
                        ColorMin.G,
                        ColorMin.B
                    );
            }
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            Bitmap resultImage = new Bitmap(sourceImage.Width, sourceImage.Height);

            errosing = true;
            for (int i = 0; i < sourceImage.Width; i++)
            {
                worker.ReportProgress((int)((float)i / resultImage.Width * 100));
                if (worker.CancellationPending) return null;

                for (int j = 0; j < sourceImage.Height; j++)
                {
                    resultImage.SetPixel(i, j, calculateNewPixelColor(sourceImage, i, j));
                }
            }

            errosing = false;
            for (int i = 0; i < sourceImage.Width; i++)
            {
                worker.ReportProgress((int)((float)i / resultImage.Width * 100));
                if (worker.CancellationPending) return null;

                for (int j = 0; j < sourceImage.Height; j++)
                {
                    resultImage.SetPixel(i, j, calculateNewPixelColor(sourceImage, i, j));
                }
            }

            for (int i = 0; i < sourceImage.Width; i++)
            {
                worker.ReportProgress((int)((float)i / resultImage.Width * 100));
                if (worker.CancellationPending) return null;

                for (int j = 0; j < sourceImage.Height; j++)
                {
                    Color color = resultImage.GetPixel(i, j);
                    double intensity = (0.36 * color.R + 0.53 * color.G + 0.11 * color.B);

                    resultImage.SetPixel(i, j, Color.FromArgb(
                            Clamp(sourceImage.GetPixel(i, j).R - (int)intensity, 0, 255),
                            Clamp(sourceImage.GetPixel(i, j).G - (int)intensity, 0, 255),
                            Clamp(sourceImage.GetPixel(i, j).B - (int)intensity, 0, 255)
                        )
                        );
                }
            }

            return resultImage;
        }
    }

    class LiniarStretchingFilter : Filters {
        int Rmax = 0, Rmin = 255, Gmax = 0, Gmin = 255, Bmax = 0, Bmin = 255;
        int difR = 255, difG = 255, difB = 255;

        public LiniarStretchingFilter(Bitmap src) {
            int ImWidth = src.Width, ImHeight = src.Height;
            int tmpR, tmpG, tmpB;

            for (int i = 0; i < ImWidth; i++)
                for (int j = 0; j < ImHeight; j++) {
                    tmpR = src.GetPixel(i, j).R;
                    if (tmpR > Rmax) Rmax = tmpR;
                    if (tmpR < Rmin) Rmin = tmpR;

                    tmpG = src.GetPixel(i, j).G;
                    if (tmpG > Gmax) Gmax = tmpG;
                    if (tmpG < Gmin) Gmin = tmpG;

                    tmpB = src.GetPixel(i, j).B;
                    if (tmpB > Bmax) Bmax = tmpB;
                    if (tmpB < Bmin) Bmin = tmpB;
                }
            difR = Rmax - Rmin;
            difG = Gmax - Gmin;
            difB = Bmax - Bmin;
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            Color tmp = sourceImage.GetPixel(x, y);

            return Color.FromArgb(
                Clamp((tmp.R - Rmin) * (255 / difR), 0, 255),
                Clamp((tmp.G - Gmin) * (255 / difG), 0, 255),
                Clamp((tmp.B - Bmin) * (255 / difB), 0, 255)
                );
        }
    }

    class GreyWorldFilter : Filters {
        double Ravg = 128, Gavg = 128, Bavg = 128;
        double Avg = 128;
        double Rcoeff = 1, Gcoeff = 1, Bcoeff = 1;
        public GreyWorldFilter(Bitmap sourceImage) {
            double Rsum = 0, Gsum = 0, Bsum = 0;
            double ImageSize = sourceImage.Width * sourceImage.Height;

            for (int i = 0; i < sourceImage.Height; i++)
            {
                for (int j = 0; j < sourceImage.Width; j++)
                {
                    Rsum += sourceImage.GetPixel(j, i).R;
                    Gsum += sourceImage.GetPixel(j, i).G;
                    Bsum += sourceImage.GetPixel(j, i).B;
                }
            }
            Ravg = Rsum / ImageSize;
            Gavg = Gsum / ImageSize;
            Bavg = Bsum / ImageSize;

            Avg = (Ravg + Gavg + Bavg) / 3.0;

            Rcoeff = Ravg / Avg;
            Bcoeff = Bavg / Avg;
            Gcoeff = Gavg / Avg;
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            return Color.FromArgb(
                Clamp((int)(sourceImage.GetPixel(x, y).R * Rcoeff), 0, 255),
                Clamp((int)(sourceImage.GetPixel(x, y).G * Gcoeff), 0, 255),
                Clamp((int)(sourceImage.GetPixel(x, y).B * Bcoeff), 0, 255)
                );    
        }
    }

    class IdealReflectorFilter : Filters {
        double Rcoeff = 0, Gcoeff = 0, Bcoeff = 0;
        public IdealReflectorFilter(Bitmap sourceImage) {
            int Rmax = 0, Gmax = 0, Bmax = 0;

            for (int i = 0; i < sourceImage.Height; i++) {
                for (int j = 0; j < sourceImage.Width; j++) {
                    int R = sourceImage.GetPixel(j, i).R,
                        G = sourceImage.GetPixel(j, i).G,
                        B = sourceImage.GetPixel(j, i).B;
                    if (R > Rmax) Rmax = R;
                    if (G > Gmax) Gmax = G;
                    if (B > Bmax) Bmax = B;
                } 
            }

            Rcoeff = 255.0 / Rmax;
            Gcoeff = 255.0 / Gmax;
            Bcoeff = 255.0 / Bmax;

        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            return Color.FromArgb(
                Clamp((int)(sourceImage.GetPixel(x, y).R * Rcoeff), 0, 255),
                Clamp((int)(sourceImage.GetPixel(x, y).G * Rcoeff), 0, 255),
                Clamp((int)(sourceImage.GetPixel(x, y).B * Rcoeff), 0, 255)
                );
        }
    }

    class CorrectionWithSupportElementFilter : Filters {
        int Rcoeff = 1, Gcoeff = 1, Bcoeff = 1; 
        public CorrectionWithSupportElementFilter(Bitmap sourceImage, int x, int y, int Rdst, int Gdst, int Bdst) {
            int Rsrc = sourceImage.GetPixel(x, y).R,
                Gsrc = sourceImage.GetPixel(x, y).G,
                Bsrc = sourceImage.GetPixel(x, y).B;

            if (Rsrc == 0) Rsrc = 1;
            if (Gsrc == 0) Gsrc = 1;
            if (Bsrc == 0) Bsrc = 1;

            Rcoeff = Rdst / Rsrc;
            Gcoeff = Gdst / Gsrc;
            Bcoeff = Bdst / Bsrc;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            return Color.FromArgb(
                Clamp(sourceImage.GetPixel(x, y).R * Rcoeff, 0, 255),
                Clamp(sourceImage.GetPixel(x, y).G * Gcoeff, 0, 255),
                Clamp(sourceImage.GetPixel(x, y).B * Bcoeff, 0, 255)
                );
        }
    }

    class NewFilter : Filters {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            double partOfWidth = sourceImage.Width / 16;

            if ((x <= 9 * partOfWidth) && (x >= 7 * partOfWidth))
            {
                double resultx = x + 20 * Math.Sin(2 * Math.PI * y / 60);

                if ((resultx < sourceImage.Width) && (resultx > -1)) return sourceImage.GetPixel((int)resultx, y);
                else return sourceImage.GetPixel((int)Math.Abs(resultx) % sourceImage.Width, y);
            }
            if (x < 7 * partOfWidth) {
                Color sourceColor = sourceImage.GetPixel(x, y);
                int Intensity = (int)(0.36 * sourceColor.R + 0.53 * sourceColor.G + 0.11 * sourceColor.B);
                Color resultColor = Color.FromArgb(Intensity, Intensity, Intensity);

                return resultColor;
            }
            if (x > 9 * partOfWidth)
            {
                Color sourceColor = sourceImage.GetPixel(x, y);
                Color resultColor = Color.FromArgb(255 - sourceColor.R, 255 - sourceColor.G, 255 - sourceColor.B);

                return resultColor;
            }
            else return sourceImage.GetPixel(x, y);
        }
    }

    class NewestFilter : Filters {
        int[] leftBorder, rightBorder;
        public NewestFilter(Bitmap sourceImage) {
            leftBorder = new int[sourceImage.Height];
            rightBorder = new int[sourceImage.Height];

            double partOfWidth = sourceImage.Width / 8;
            for (int i = 0; i < sourceImage.Height; i++ )
            {
                leftBorder[i] = Clamp((int)(partOfWidth * 3 + 20 * Math.Sin(2 * Math.PI * i / 60)), 0, 255);
                rightBorder[i] = Clamp((int)(partOfWidth * 5 + 20 * Math.Sin(2 * Math.PI * i / 60)), 0, 255);
            }
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y, int offset = 0)
        {
            double resultx = x + 20 * Math.Sin(2 * Math.PI * y / 60);


                if ((x >= leftBorder[y]) && (x <= rightBorder[y]) && (resultx < sourceImage.Width) && (resultx > -1))
                {
                    return sourceImage.GetPixel((int)resultx, y);
                }

                if ((x < leftBorder[y]) || (resultx < - 1))
                {
                    Color sourceColor = sourceImage.GetPixel(x, y);
                    int Intensity = (int)(0.36 * sourceColor.R + 0.53 * sourceColor.G + 0.11 * sourceColor.B);
                    Color resultColor = Color.FromArgb(Intensity, Intensity, Intensity);

                    return resultColor;
                }
                if ((x > rightBorder[y])  || (resultx > sourceImage.Width))
                {
                    Color sourceColor = sourceImage.GetPixel(x, y);
                    Color resultColor = Color.FromArgb(255 - sourceColor.R, 255 - sourceColor.G, 255 - sourceColor.B);

                    return resultColor;
                }
                else return sourceImage.GetPixel(x, y);
            
            //else return sourceImage.GetPixel((int)Math.Abs(resultx) % sourceImage.Width, y);
        }
    }
}



